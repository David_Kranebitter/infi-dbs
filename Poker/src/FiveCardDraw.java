import java.sql.SQLException;
import java.util.Arrays;
import java.util.Scanner;

public class FiveCardDraw {

	static final int AMOUNTOFSAMECOLORS = 13;
	static final int AMOUNTOFCOLORS = 4;
	static int[] hand = new int[5];
	static int[] res = new int[8];
	static double[] resPerc = new double[8];
	static Scanner s = new Scanner(System.in);
	static int runs = 0;

	public static void main(String[] args) throws SQLException {

		PokerDBManager db = new PokerDBManager();
		boolean rep = true;
		boolean err = false;

		while (rep) {
			do {
				String st;
				System.out.println("Bitte Anzahl der Vorgaenge eingeben: ");
				st = s.next();
				if (isInt(st)) {
					err = false;
					runs = Integer.parseInt(st);
				} else {
					System.out.println("Ungueltige Eingabe!");
					err = true;
					System.out.println("Bitte erneut eingeben: ");
				}

			} while (err);
			long time = System.currentTimeMillis();

			for (int i = 0; i < runs; i++) {

				newDraw();
				switch (countSameCards(hand)) {
				case 1:
					res[0]++;
					break;
				case 2:
					res[1]++;
					break;
				case 3:
					res[2]++;
					break;
				case 6:
					res[3]++;
					break;
				case 4:
					res[5]++;
					break;
				}

				if (flush(hand)) {
					res[4]++;
				}
				if (straight(hand)) {
					res[6]++;
				}
				if (straigthFlush(hand)) {
					res[7]++;
				}

			}
			long time2 = System.currentTimeMillis();
			long resTime = time2 - time;
			resPerc = getPercentage(res);
			db.createTable();
			db.addResults(time2, runs, res);
			
			System.out.println("Dauer: " + resTime + " ms");
			System.out.println("1 Pair: " + res[0] + "\t        " + resPerc[0]);
			System.out.println("2 Pairs: " + res[1] + "\t       " + resPerc[1]);
			System.out.println("Triple: " + res[2] + "\t        " + resPerc[2]);
			System.out.println("Four of a kind: " + res[3] + "\t" + resPerc[3]);
			System.out.println("Flush: " + res[4] + "\t         " + resPerc[4]);
			System.out.println("Full House: " + res[5] + "\t    " + resPerc[5]);
			System.out.println("Straight: " + res[6] + "\t      " + resPerc[6]);
			System.out.println("Straight Flush: " + res[7] + "\t" + resPerc[7]);
			System.out.println();
			System.out.println("Erneut?: [N/J]");

			do {
				switch (s.next()) {
				case "J":
					err = false;
					break;

				case "N":
					err = false;
					rep = false;
					db.close();
					break;

				default:
					System.out.println("Ungueltige Eingabe!");
					err = true;
					System.out.println("Bitte erneut eingeben: ");
				}
			} while (err);
		}
	}

	public static boolean isInt(String in) {
		for (int i = 0; i < in.length(); i++) {
			if (!(Character.isDigit(in.charAt(i)))) {
				return false;
			}
		}
		return true;
	}

	public static double[] getPercentage(int[] res) {
		double[] arr = new double[8];

		for (int i = 0; i < res.length; i++) {
			arr[i] = res[i] * 100.0 / runs;
		}
		return arr;
	}

	public static int cardValue(int card) {
		return card % AMOUNTOFSAMECOLORS;
	}

	public static int cardColor(int card) {
		return card / AMOUNTOFCOLORS;
	}

	public static void newDraw() {
		int ran = 0;
		int swap = 0;

		for (int i = 0; i < 5; i++) {
			int[] cards = new int[AMOUNTOFSAMECOLORS * AMOUNTOFCOLORS];
			ran = (int) (Math.random() * (52 - i));
			for (int j = 0; j < 52; j++) {
				cards[j] = j + 1;
			}
			hand[i] = cards[ran];
			cards[51 - i] = swap;
			cards[51 - i] = cards[ran];
			cards[ran] = swap;
		}

	}

	public static int countSameCards(int[] hand) {
		int iCounter = 0;
		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = i + 1; j < hand.length; j++) {
				if (cardValue(hand[i]) == cardValue(hand[j])) {
					iCounter++;
				}
			}
		}
		return iCounter;

	}

	public static boolean flush(int[] hand) {
		for (int i = 0; i < hand.length - 1; i++) {
			if (cardColor(hand[i]) != cardColor(hand[i + 1])) {
				return false;
			}
		}
		return true;
	}

	public static boolean straight(int[] hand) {
		Arrays.sort(hand);
		for (int i = 1; i < hand.length - 1; i++) {
			if (cardValue(hand[i]) - cardValue(hand[i - 1]) != 1) {
				return false;
			}
		}
		return true;
	}

	public static boolean fullHouse(int[] hand) {
		int comp = 0;
		boolean a = false;
		boolean c = false;
		for (int i = 0; i < hand.length - 2; i++) {

			if (cardValue(hand[i]) == cardValue(hand[i + 1]) && cardValue(hand[i + 1]) == cardValue(hand[i + 2])) {
				a = true;
				comp = cardValue(hand[i]);
			}

		}
		for (int i = 0; i < hand.length - 1; i++) {
			if (cardValue(hand[i]) == cardValue(hand[i + 1]) && cardValue(hand[i]) != comp) {
				c = true;
			}

		}
		return c && a;
	}

	public static boolean straigthFlush(int[] hand) {
		boolean b = true;
		boolean a = true;
		Arrays.sort(hand);

		for (int i = 1; i < hand.length - 1; i++) {
			if (cardValue(hand[i]) - cardValue(hand[i - 1]) != 1) {
				b = false;
			}
		}

		for (int i = 0; i < hand.length - 1; i++) {
			if (cardColor(hand[i]) != cardColor(hand[i + 1])) {
				a = false;
			}
		}
		return b && a;
	}
}
