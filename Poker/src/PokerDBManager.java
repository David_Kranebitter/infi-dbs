import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PokerDBManager {

	private Connection c;
	private static PokerDBManager db;

	PokerDBManager() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/sqlite-tools-win32-x86-3150200/poker.db");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static PokerDBManager getInstance() throws SQLException {
		if (db == null) {
			db = new PokerDBManager();
		}
		return db;
	}


	public void createTable() throws SQLException{
		PreparedStatement stmt = null;
		String sql = "CREATE TABLE IF NOT EXISTS results(time INT primary key,"
				      + "runs INT NOT NULL, onePair INT NOT NULL, twoPairs INT NOT NULL,"
				      + "threeOfAKind INT NOT NULL, fourOfAKind INT NOT NULL, "
				      + "flush INT NOT NULL, straight INT NOT NULL, "
				      + "straightFlush INT NOT NULL);";
		stmt = c.prepareStatement(sql);
		stmt.executeUpdate();
		stmt.close();
	}
	
	public void addResults(long time, int runs, int[] results)
	{
		PreparedStatement stmt = null;
		String sql = "INSERT INTO results VALUES (?,?,?,?,?,?,?,?,?)";
		try {
			stmt = c.prepareStatement(sql);
			stmt.setLong(1, time);
			stmt.setInt(2, runs);
			stmt.setInt(3, results[0]);
			stmt.setInt(4, results[1]);
			stmt.setInt(5, results[2]);
			stmt.setInt(6, results[3]);
			stmt.setInt(7, results[4]);
			stmt.setInt(8, results[5]);
			stmt.setInt(9, results[6]);
			stmt.executeUpdate();
		}catch(SQLException e)
		{
			e.printStackTrace();
		}finally {
				if(stmt != null)
				{
					try{
						stmt.close();
					}catch (SQLException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		
		public void close()
		{
			try{
				c.close();
			}catch (SQLException e){
				e.printStackTrace();
			}
		}
	}

