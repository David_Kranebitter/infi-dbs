import static org.junit.Assert.*;

import org.junit.Test;

public class CountSameCardsTest {

	@Test
	public void test() {
		FiveCardDraw f = new FiveCardDraw();
		int[] arr = {46,38,1,2,25};
		int i = f.countSameCards(arr);
		assertEquals(1, i);
	}

}
