import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CountSameCardsTest.class, FlushTest.class, FullHouseTest.class, straightFlushTest.class,
		straightTest.class })
public class AllTests {

}
