import static org.junit.Assert.*;

import org.junit.Test;

public class FlushTest {

	@Test
	public void test() {
		FiveCardDraw f = new FiveCardDraw();
		int[] arr = {1,2,3,4,5};
		boolean b = f.flush(arr);
		assertEquals(true, b);
	}

}
