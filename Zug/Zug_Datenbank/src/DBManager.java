import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

public class DBManager {

	private static Connection c;
	private static DBManager db;
	private static Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/sqlite-tools-win32-x86-3150200/Zug.db");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static DBManager getInstance() throws SQLException {
		if (db == null) {
			db = new DBManager();
		}
		return db;
	}

	public static void create() throws SQLException {
		String sql1 = "CREATE TABLE IF NOT EXISTS Kunde(KundeID INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "Name TEXT NOT NULL, " + "sitzplatz INTEGER NOT NULL);";

		String sql2 = "CREATE TABLE IF NOT EXISTS Wagon(WagonID INTEGER PRIMARY KEY AUTOINCREMENT,	"
				+ "Klasse INTEGER NOT NULL);";

		String sql3 = "CREATE TABLE IF NOT EXISTS Reservierung(Sitzplatz INTEGER, " + "WagonID INTEGER, "
				+ "Primary key(Sitzplatz, WagonID));";

		PreparedStatement stmt = null;
		stmt = c.prepareStatement(sql1);
		stmt.executeUpdate();

		stmt = c.prepareStatement(sql2);
		stmt.executeUpdate();

		stmt = c.prepareStatement(sql3);
		stmt.executeUpdate();
		stmt.close();
	}

	public static void insert() throws SQLException {
		System.out.println("Bitte den Namen der Tabelle eingeben, in der sie Daten einfuegen wollen: ");
		PreparedStatement stmt = null;
		String s = sc.next();
		String insert1 = "INSERT INTO " + s + " VALUES (?,?,?)";
		String insert2 = "INSERT INTO " + s + " VALUES (?,?)";

		if (s.equals("Kunde")) {
			stmt = c.prepareStatement(insert1);

			System.out.println("Bitte Name eingeben:");
			stmt.setString(2, sc.next());
			System.out.println("Bitte Sitzplatz eingeben: ");
			stmt.setInt(3, sc.nextInt());

		}
		if (s.equals("Wagon")) {
			stmt = c.prepareStatement(insert2);

			System.out.println("Bitte Klasse eingeben:");
			stmt.setInt(2, sc.nextInt());
		}
		if (s.equals("Reservierung")) {
			stmt = c.prepareStatement(insert2);

			System.out.println("Bitte Sitzplatz eingeben: ");
			stmt.setInt(1, sc.nextInt());
			System.out.println("Bitte KundenID eingeben: ");
			stmt.setInt(2, sc.nextInt());
		}
		stmt.executeUpdate();
		stmt.close();
	}

	public static void delete() {
		PreparedStatement stmt = null;
		System.out.println("Bitte den Namen der Tabelle eingeben und ID: ");
		String st = sc.next();
		int id = sc.nextInt();

		String delete1 = "DELETE FROM " + st + " WHERE KundeID = " + id;
		String delete2 = "DELETE FROM " + st + " WHERE WagonID = " + id;

		try {
			if (st.equals("Kunde")) {
				stmt = c.prepareStatement(delete1);
				stmt.executeUpdate();
			}
			if (st.equals("Wagon")) {
				stmt = c.prepareStatement(delete2);
				stmt.executeUpdate();
			}
			if (st.equals("Reservierung")) {
				stmt = c.prepareStatement(delete1);
				stmt.executeUpdate();
			}
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public static void show() throws SQLException {
		System.out.println("Von welcher Tabelle sollen die Daten ausgegeben werden? ");
		String st = sc.next();
		String sql = "SELECT * FROM " + st + ";";

		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {

			if (st.equals("Kunde")) {
				System.out.println("--------------------------------------------------");
				System.out.println("KundenID: " + rs.getInt(1));
				System.out.println("Name: " + rs.getString(2));
				System.out.println("Sitzplatz: " + rs.getString(3));
				System.out.println("--------------------------------------------------");

			}
			if (st.equals("Wagon")) {
				System.out.println("--------------------------------------------------");
				System.out.println("WagonID: " + rs.getInt(1));
				System.out.println("Klasse: " + rs.getInt(2));
				System.out.println("--------------------------------------------------");

			}
			if (st.equals("Reservierung")) {
				System.out.println("--------------------------------------------------");
				System.out.println("Sitzplatz: " + rs.getInt(1));
				System.out.println("WagonID: " + rs.getInt(2));
				System.out.println("--------------------------------------------------");

			}
		}
		rs.close();
		stmt.close();
	}
	
	public static int peopleInTrain() throws SQLException{
		int iCounter=0;
		PreparedStatement stmt = null;
		String sql = "SELECT COUNT(DISTINCT KundeID) FROM Kunde;";
		stmt = c.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		System.out.println();
		
		while(rs.next()){
			iCounter = rs.getInt(1);
		}
		rs.close();
		stmt.close();
		return iCounter;
	}
	
	public static int peopleInClass() throws SQLException{
		int people = 0;
		System.out.println("Bitte Klasse eingeben: ");
		int in = sc.nextInt();
		String sql = "select COUNT(Sitzplatz) from Reservierung "+ 
					  "inner join Wagon on Reservierung.WagonID = Wagon.WagonID "+
				      "where Wagon.Klasse = " + in;

		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()){
			people = rs.getInt(1);
		}
		
		return people;
	}
	
	public static int nameInClass() throws SQLException{
		int klasse = 0;
		System.out.println("Bitte den Namen des Kunden eingeben: ");
		String st = sc.next();
		String sql = "select Klasse from Wagon "+ 
					  "left join Reservierung on Wagon.WagonID = Reservierung.WagonID "+
					  "left join Kunde on Reservierung.Sitzplatz = Kunde.Sitzplatz "+
				      "where Kunde.name = " + "'" + st + "'";

		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);
		
		while(rs.next()){
			klasse = rs.getInt(1);
		}
		
		return klasse;
	}

	public static void main(String[] args) throws SQLException {
		getInstance();
		create();

		while (true) {
			System.out.println("------------------------------------");
			System.out.println("Insert: 1");
			System.out.println("Delete: 2");
			System.out.println("Show:   3");
			System.out.println("Show how many people: 4");
			System.out.println("Wich name in which class: 5");
			System.out.println("Show how wany people are in which class: 6");

			int choice = sc.nextInt();
			switch (choice) {

			case 1:
				insert();
				break;
			case 2:
				delete();
				break;
			case 3:
				show();
				break;
			case 4:
				System.out.println(peopleInTrain());
				break;
			case 5:
				System.out.println(nameInClass());
				break;
			case 6:
				System.out.println(peopleInClass());
				break;
			}
		}
	}

}
