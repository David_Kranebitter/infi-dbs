import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBManager {

	private static String sql1 = "CREATE TABLE IF NOT EXISTS Song(SongID INT primary key," + "Name TEXT NOT NULL,"
			+ "Dauer INT NOT NULL," + " Musikrichtung TEXT NOT NULL);";
	private static String sql2 = "CREATE TABLE IF NOT EXISTS CD(CDID INT primary key," + "Preis INT NOT NULL,"
			+ "Label TEXT NOT NULL," + "Jahr INT NOT NULL);";
	private static String sql3 = "CREATE TABLE IF NOT EXISTS Interpret(InterpretID INT primary key,"
			+ "Vorname TEXT NOT NULL," + "Nachname TEXT NOT NULL," + "Geburtstag INT NOT NULL);";

	private static Connection c;
	private static DBManager db;
	private static Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:/sqlite/sqlite-tools-win32-x86-3150200/Musik.db");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static DBManager getInstance() throws SQLException {
		if (db == null) {
			db = new DBManager();
		}
		return db;
	}

	public static void createTable(String SQL) throws SQLException {
		PreparedStatement stmt = null;
		stmt = c.prepareStatement(SQL);
		stmt.executeUpdate();
		stmt.close();
	}

	public static void insert() {
		int tableNr = 0;
		System.out.println("In welche Tabelle wollen sie Daten einf�gen?");
		PreparedStatement stmt = null;
		String s = sc.next();
		String sql = "INSERT INTO " + s + " VALUES (?,?,?,?)";
		switch (s) {
		case "Song":
			tableNr = 1;
			break;

		case "CD":
			tableNr = 2;
			break;

		case "Interpret":
			tableNr = 3;
			break;

		default:
			System.out.println("Ung�ltige Eingabe!");
			break;
		}

		try {
			stmt = c.prepareStatement(sql);
			if (tableNr == 1) {

				System.out.print("SongID eingeben: ");
				stmt.setInt(1, sc.nextInt());
				System.out.print("Songname einegebn: ");
				stmt.setString(2, sc.next());
				System.out.print("Songdauer eingeben: ");
				stmt.setInt(3, sc.nextInt());
				System.out.print("Musikrichtung einegben: ");
				stmt.setString(4, sc.next());
			}
			if (tableNr == 2) {
				System.out.print("CDID eingeben: ");
				stmt.setInt(1, sc.nextInt());
				System.out.print("Preis einegebn: ");
				stmt.setInt(2, sc.nextInt());
				System.out.print("Label eingeben: ");
				stmt.setString(3, sc.next());
				System.out.print("Jahr einegben: ");
				stmt.setInt(4, sc.nextInt());
			}
			if (tableNr == 3) {
				System.out.print("InterpretID eingeben: ");
				stmt.setInt(1, sc.nextInt());
				System.out.print("Vorname einegebn: ");
				stmt.setString(2, sc.next());
				System.out.print("Nachname eingeben: ");
				stmt.setString(3, sc.next());
				System.out.print("Jahr einegben: ");
				stmt.setInt(4, sc.nextInt());
			}
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void delete() throws SQLException {
		int tableNr = 0;
		PreparedStatement stmt = null;
		System.out.println("Welche Tabelle soll gel�scht werden?: ");
		String s = sc.next();
		System.out.println("Bitte ID eingeben: ");
		int id = sc.nextInt();

		String sql1 = "DELETE FROM " + s + " WHERE SongID = " + id;
		String sql2 = "DELETE FROM " + s + " WHERE CDID = " + id;
		String sql3 = "DELETE FROM " + s + " WHERE InterpretID = " + id;

		switch (s) {
		case "Song":
			tableNr = 1;
			break;

		case "CD":
			tableNr = 2;
			break;

		case "Interpret":
			tableNr = 3;
			break;

		default:
			System.out.println("Ung�ltige Eingabe!");
			break;
		}

		try {

			if (tableNr == 1) {
				stmt = c.prepareStatement(sql1);
				stmt.executeUpdate();
			}
			if (tableNr == 2) {
				stmt = c.prepareStatement(sql2);
				stmt.executeUpdate();
			}
			if (tableNr == 3) {
				stmt = c.prepareStatement(sql3);
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void show() throws SQLException {
		int tableNr = 0;
		System.out.println("Von welcher Tabelle sollen die Daten ausgegeben werden? ");
		String s = sc.next();
		String sql = "SELECT * FROM " + s + ";";
		switch (s) {
		case "Song":
			tableNr = 1;
			break;

		case "CD":
			tableNr = 2;
			break;

		case "Interpret":
			tableNr = 3;
			break;

		default:
			System.out.println("Ung�ltige Eingabe!");
		}

		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		System.out.println("-------------------------------------------------");
		System.out.println("Tabelle: " + rs.getMetaData().getTableName(tableNr) + "");
		System.out.println("-------------------------------------------------");
		while (rs.next()) {
			if (tableNr == 1) {
				System.out.println("SongID: " + rs.getInt(1));
				System.out.println("Songname: " + rs.getString(2));
				System.out.println("Songdauer: " + rs.getInt(3));
				System.out.println("Musikrichtung: " + rs.getString(4));
			}
			if (tableNr == 2) {
				System.out.println("CDID: " + rs.getInt(1));
				System.out.println("Preis: " + rs.getInt(2));
				System.out.println("Label: " + rs.getString(3));
				System.out.println("Jahr: " + rs.getInt(4));
			}
			if (tableNr == 3) {
				System.out.println("InterpretID: " + rs.getInt(1));
				System.out.println("Vorname: " + rs.getString(2));
				System.out.println("Nachname: " + rs.getString(3));
				System.out.println("Geburtstag: " + rs.getInt(4));
			}
			System.out.println("-------------------------------------------------");
		}

		rs.close();
		stmt.close();
	}

	public static void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void menu() throws SQLException {
		System.out.println("Verfuegbare Tabellen: Song, CD, Interpret");
		System.out.println();
		System.out.println("delete ... d");
		System.out.println("insert ... i");
		System.out.println("show ... s");
		System.out.println("close ... c");
		System.out.println();
		
		String s = sc.next();
		
		switch(s){
		case "s": show();
			break;
		
		case "i": insert();
			break;
			
		case "d": delete();
			break;
			
		case "c": close();	
			
		default: System.out.println("Ung�ltige Eingabe!");
			break;
		}
		System.out.println();
	}

	public static void main(String[] args) throws SQLException {
		getInstance();

		while (true) {
		menu();
		}

	}
}
